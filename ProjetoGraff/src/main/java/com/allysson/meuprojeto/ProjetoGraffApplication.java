package com.allysson.meuprojeto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoGraffApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoGraffApplication.class, args);
	}

}
