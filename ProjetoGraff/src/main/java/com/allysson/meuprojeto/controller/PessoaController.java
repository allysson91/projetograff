package com.allysson.meuprojeto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.allysson.meuprojeto.model.Pessoa;
import com.allysson.meuprojeto.repository.PessoaRepository;

@Controller
public class PessoaController {

	@Autowired
	private PessoaRepository pr;

	@RequestMapping(value = "/cadastrarPessoa", method = RequestMethod.GET)
	public String form() {
		return "pessoa/formPessoa";
	}

	@RequestMapping(value = "/cadastrarPessoa", method = RequestMethod.POST)
	public String form(Pessoa pessoa) {

		pr.save(pessoa);
		return "redirect:/cadastrarPessoa";
	}

	@RequestMapping("/pessoas")
	public ModelAndView listaPessoas() {
		ModelAndView mv = new ModelAndView("tablePessoa");
		Iterable<Pessoa> pessoas = pr.findAll();
		mv.addObject("pessoas", pessoas);
		return mv;
	}
}
