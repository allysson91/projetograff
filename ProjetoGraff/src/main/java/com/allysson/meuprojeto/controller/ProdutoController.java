package com.allysson.meuprojeto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.allysson.meuprojeto.model.Produto;
import com.allysson.meuprojeto.repository.ProdutoRepository;

@Controller
public class ProdutoController {

	@Autowired
	private ProdutoRepository pr;

	@RequestMapping(value = "/cadastrarProduto", method = RequestMethod.GET)
	public String form() {
		return "produto/formProduto";
	}

	@RequestMapping(value = "/cadastrarProduto", method = RequestMethod.POST)
	public String form(Produto produto) {

		pr.save(produto);
		return "redirect:/cadastrarProduto";
	}

	@RequestMapping("/produtos")
	public ModelAndView listaProdutos() {
		ModelAndView mv = new ModelAndView("tableProduto");
		Iterable<Produto> produtos = pr.findAll();
		mv.addObject("produtos", produtos);
		return mv;
	}

	@RequestMapping("/{id}")
	public ModelAndView detalheProduto(@PathVariable("id") Integer id) {
		Produto produto = pr.findById(id);
		ModelAndView mv = new ModelAndView("produto/detalhesProduto");
		mv.addObject("produto", produto);
		return mv;
	}

}
