package com.allysson.meuprojeto.repository;

import org.springframework.data.repository.CrudRepository;

import com.allysson.meuprojeto.model.Pessoa;

public interface PessoaRepository extends CrudRepository<Pessoa, String> {

}
