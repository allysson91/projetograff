package com.allysson.meuprojeto.repository;

import org.springframework.data.repository.CrudRepository;

import com.allysson.meuprojeto.model.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, String> {

	Produto findById(Integer id);

}
